# Report

## Abstract

This is an example code based on a simple N-body simulation of a distribution of point masses placed
at location r_1,...,r_N and have masses m_1,...,m_N. The position of the particles after a specified time is computed using a finite difference methods for ordinary differential equation.

This report will focus on improving run time of this code mostly based on Intel Advisor and partly based on Intel Vtune.

## Optimization process

### Initial run time

![inital](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/bb9456234ca036538f41fe91f4991c40/image.png)

### Use AVX512 2b19621b52089ba7bb9ec14a50b0f8c0e5f12a96

![advisor output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/b27889fdcbdaf8abace0ebac6ff2f12e/image.png)

Due to backwards competability (**TODO: CHECK**), intel's avx512 is not enabled by default. Therefore, in order to use avx512 registers, one must enable this feature during compilation. 

run time: (improvement: X3.05)

![output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/1ca8150fb077521e7d4e1059ea8c63d6/image.png)

### Set zmm registers usage to high 8f085c699f7556b884a45f4e50d8a836e8bed017

![advisor output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/fda9e9c0d3045076a7bd4b91d343d1a2/image.png)

One can choose whether to use the zmm registers often or not by setting -qopt-zmm-usage=high (or low). As advisor suggested, I set zmm usage to high.

run time: (no improvement)

![output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/3cf407c3f88f6eecb0b5c13bd02d38a0/image.png)

### Convert particles to SoA 009d1a3b6bb09d815682ce29f8c6a859ebda097e ###

Running memory access patern analysis (MAP) on advisor yields the following output:

![advisor output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/7162b04421aaa55e140a94b1c4d63f92/image.png)

As one can see, we access data in non-uniform strides, while ideally the strides size should be 1. When using vectorization
it can cause a major slow down in run time. 

The reason for the non-uniform strides is the particles stracture. During the for-loop, we go from one particle to another
while the particle structure size is not negligible.

```
struct Particle
{
  real_type pos[3];
  real_type vel[3];
  real_type acc[3];
  real_type mass;
};
```

Converting particle to SoA allowed access in strides of 1

```
struct ParticleSoA
{
public:
  real_type *pos_x, *pos_y, *pos_z;
  real_type *vel_x, *vel_y, *vel_z;
  real_type *acc_x, *acc_y, *acc_z;
  real_type *mass;
};
```

When I converted to ParticleSoA, I noticed a slow down in run time compared to Particle, and vectorization was canceled. As seen on advisor, it was canceled due to dependencies. So I ran dependencies analysis on vtune in order to see which dependencies are real and how wo solve them.

![advisor output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/ef0245ba2bb51e61f0722dee41e43216/image.png)

We can see 3 real dependencies and 1 unreal dependency. The real dependencies were from [here](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/009d1a3b6bb09d815682ce29f8c6a859ebda097e#32d8e099ad6684ad5562afe4773e4536f925e01b_148_156), and can be solved in two ways: reduction or change loops order (the chosen way as shown [here](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/38ea6aecd6c92c7b7f2e19a0581accfb12d88d0f#32d8e099ad6684ad5562afe4773e4536f925e01b_142_141) and [here](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/38ea6aecd6c92c7b7f2e19a0581accfb12d88d0f#32d8e099ad6684ad5562afe4773e4536f925e01b_144_144)).
Then, we remain with the one unreal dependency as shown here (after running another dependency check):

![advisor output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/21bd117f70732799d56b4b5c52842213/image.png)

so we can tell the compiler to ignore this dependecy using #pragma omp simd as written [here](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/38ea6aecd6c92c7b7f2e19a0581accfb12d88d0f#32d8e099ad6684ad5562afe4773e4536f925e01b_144_143).

run time for 38ea6aecd6c92c7b7f2e19a0581accfb12d88d0f:

![output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/e238057db35eab225fb1a5b06b106c33/image.png)

### Resolve type conversions 60f1f6c504494e0dbcd2b31254e639d598bb16fa ###

When using vectorization, type conversions might cause major unnecessary slow down in run time. Therefore, one should avoid type conversions and be aware of implicit ones. For example, libc's sqrt function has three implementations: 
```
double sqrt(double x);
float sqrtf(float x);
long double sqrtl(long double x);
```
When calling sqrt(x) when x is of type float, it will first be implicitly casted into type double. Therefore, it's the programmer's responsibility to call sqrtf when using floats.

Luckily, intel advisor shows when type conversions are done, as shown here:

![advisor output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/97bdba47f0ae85a246cd820c048bed6d/image.png)

In our case, solving all type conversions and working with floats only yields the following run time:

![output](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/b084976c41f4d64e1d5cb8f3970c149e/image.png)

### Align memory 3e4f17eabf9ffdfc1dae4be4fc63345f14bd8361 ###

When using vectorization, loops might be splitted into 3 parts:
- peel loop: taking care of the first iterations until the data accessed in the loop is aligned
- main loop: taking of the "middle" iterations. Accesss are aligned and vectorized.
- remainder loop: taking care of the remainder (last) iterations. The data part that doesnt fill a whole vector.

When possible, it is importrant to get rid of the peel loop part as long as we can.
[This commit](3e4f17eabf9ffdfc1dae4be4fc63345f14bd8361) and the [previous commit](78923557d988181c0944d84d46d78001cebd8568) are both based on code analytics section after running roofline with marked loop:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/de0de843cb9d94b085f17184450af435/image.png)

After using aligned mallocs [here](78923557d988181c0944d84d46d78001cebd8568), I still got the same notes about unalign access. It happened due to the fact that the compiler still goes on the "safe side" and assumes the stractures are unaligned. So I had to tell the compiler explicitly to assume the stractures are aligned, as done in this [commit](3e4f17eabf9ffdfc1dae4be4fc63345f14bd8361). And now the note about unaligned access is gone:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/7573c4e81783b9f2e174aef57d569914/image.png)

run time:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/d4b0e3b84ebaf1518e2009ba8cf8b117/image.png)

### Parallelization ###

After running vtune I could figure out that my application uses only a single core:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/d4c32a07333c63be4eca56dd341463fd/image.png)

so I added some [trivial implementation](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/7719d48b71327788923d240baf8d97ce35ac6dfa#32d8e099ad6684ad5562afe4773e4536f925e01b_230_144) of parallelism which yielded bad results:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/aab918762e352e5054dae994d61a0376/image.png)

The reason for the bad results is that the threads keep created and destroyed - n times. It can be shown on vtune:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/2951fd389cec3779ca3baa384abdca70/image.png)

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/9a314d01693b83e10503bb4939a0c168/image.png)

Then I dived deeper into the code and examined 3 different implementations:

1. 1a676d30d2747dae6ad9cac17febd5745fb221d1 - changed loops order and parallelized outer loop
2. cf6aae863f009d3b8b45b6af6c5e58db7ce4e1a8 - menually splitted inner loop to threads
3. b2b6038c96385eea94d081714049b48294da340b - created thread-private arrays and reduced them in critical section

#### 1. 1a676d30d2747dae6ad9cac17febd5745fb221d1 - changed loops order and parallelized outer loop ####

now the outer loop is parallelized (had to swap i and j), but the vectorization was cancled due to dependencies as shown on Advisor:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/8272ae95055be9fd2d0b55709d5931f4/image.png)

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/4fba62bcb531aa82c3e618a93f240af8/image.png)

and the results:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/625bbbf7f749e4a6eda5c37668d0ae02/image.png)

##### 4311c2f7875736252704aef2c82de27052bef301 #####

After I viewed in Advisor the dependencies:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/de24b4b1956ca264949e0ba5688de198/image.png)

I managed to solve these by adding [reduction pragma](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/4311c2f7875736252704aef2c82de27052bef301#32d8e099ad6684ad5562afe4773e4536f925e01b_146_145)

and now the results are much better (16 threads):

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/3440278d08f8a7ee976f06f479da2900/image.png)

#### 2. cf6aae863f009d3b8b45b6af6c5e58db7ce4e1a8 - menually splitted inner loop to threads ####

TODO: COMPLETE

results:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/64a135d80e328ff78c1a526d13a0912a/image.png)

I ran Advisor on the [previous commit](48532f4bc38206f90e51d97ba328265b77b83301) and I've seen that assumed dependencies prevents vectorization:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/872c9e87a000709a0c5af928b139e0b3/image.png)

So I ran dependencies analysis:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/c095dd7993c5b2087afbc04a6d56e96c/image.png)

I got a fake dependecy, so I added [simd pragma](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/42d269855dedaa7d8a55c00e44169e070538c810#32d8e099ad6684ad5562afe4773e4536f925e01b_147_147) on the inner loop.

Results:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/9929cec2d22df80533b3e7d8bc9ca7f1/image.png)

##### A note about the run time of this implementation #####

I got some bad vectorization efficiency that might be caused by inefficient memory access patterns:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/1b0665ae8fe70cb1da20f8746dd832f6/image.png)

So I ran MAP check:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/824977081772a56b8e89b163467e8217/image.png)

I've noticed that the reason for these MAP results are that [the lower bound and the upper bound of the inner loop are unknown at compile time](https://gitlab.com/nadavhalahmi/nbody-demo/-/commit/42d269855dedaa7d8a55c00e44169e070538c810#32d8e099ad6684ad5562afe4773e4536f925e01b_147_148) (even when I ran this commit with 1 thread, there was a major difference on run time between this code:
```
int tid = omp_get_thread_num();
int from = tid * part;
int to = std::min((tid + 1) * part, n);
```
and this code:
```
int tid = omp_get_thread_num();
int from = 0;
int to = n;
```

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/be0826a78daf5adb07cbb0872f2d0873/image.png)

A further examination revealed that only when `from` (and not `to`) is set on run time the performance is affected.

#### 3. b2b6038c96385eea94d081714049b48294da340b - created thread-private arrays and reduced them in critical section ####

TODO: COMPLETE

I examined two additional reduction methods:
1. f597e36b9633ef7d834981191016a9d5f7589e43 - 3 atomics inside for loop
2. 641cd8ac9eb04a86a227e3170f254db950aa9fc1 - moved critical section to be inside for loop

run time for this commit:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/6bd941d973a5f79948e4f2989cab2bd5/image.png)

As seen on vtune (below), a major part of the run time is spent on threading overhead. Increasing the number of particles "solved" this issue, and could be concluded from the fact that GFLOPS kept increasing on increment of number of particles.

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/e62cd68d3ad11ece862f8f7f2cfc9f6a/image.png)

run time with 20,000 particles:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/f8ae8e0084f2ac3bf6b0df39a7c0887b/image.png)

vtune analysis:

![image](https://gitlab.com/nadavhalahmi/nbody-demo/uploads/ea223391d99ce0cfaf6a4a7fcc4da74f/image.png)

